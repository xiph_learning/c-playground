#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INIT_SIZE 10

char **new_list(int len){
    char **str = (char **)malloc(sizeof(char *));
    for (int i = 0; i < len; i++){
        *(str+i) = NULL;
    }

    return str;
}

void add_string(char **str, int len, char *new_string){
    int i = 0;
    while(*str != NULL && i < len){
        i++;
    }

    if(i == len - 1){
        printf("Needs resize\n");
    }

    *(str + i) = (char *)malloc(sizeof(char) * strlen(new_string));

    if (str[i] != NULL)
        strcpy(*(str+i), new_string);

    if (*(str + i) != NULL)
        printf("%s\n", *(str + i));
}

int main() {
    int len = INIT_SIZE;
    char **str = new_list(len);

    add_string(str, len, "This is a test");

    while(*(str) != NULL){
        printf("%s\n", *str);
        str++;
    }

   //  for(int i = 0; i < len; i++)
   //      if (str[i] != NULL)
   //          printf("%s\n", str[i]);



    return 0;
}
