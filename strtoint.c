#include <stdio.h>
#include <string.h>

int strtoint(char *str) {
    int n = 0;

    for(int i = 0; i < strlen(str); i++){
        if (str[i] - 48 >= 0 && str[i] - 48 < 10)
            n = (n * 10) + ((int)str[i] - 48);
        else
            return -1;
    }

    return n;
}

int main(int argc, char **argv){
   if (argc == 1) 
        printf("%d\n", strtoint("01023"));
   else
        printf("%d\n", strtoint(argv[1]));

    return 0;
}
