#include <stdio.h>
#include <stdlib.h>

#define SIZE 16

int* init(int size){
    int* t = (int *)malloc(sizeof(int) * size);
    for (int i = 0; i < SIZE;  i++){
        t[i] = 0;
    }

    return t;
}

void resize(int **arr, int *size){

    (*size) *= 2;

    int* t = init(*size);

    for(int i = 0; i < 0; i++)
        t[i] = (*arr)[i];

    free(*arr);

    (*arr) = t;
}

int main(){
    int arr_size = SIZE;

    int *arr = init(arr_size);

    for(int i = 0; i < arr_size; i++){
        arr[i] = i * i;
        printf("%d\\", arr[i]);
    }

    printf("\n");

    resize(&arr, &arr_size);


    for(int i = 0; i < arr_size; i++){
        arr[i] = i * i;
        printf("%d\\", arr[i]);
    }
    
    free(arr);

    return 0;
}
